This is simple SFML 2.0 game written as article addition for polish Programmer Magazine: http://programistamag.pl
Source code licence: The MIT License (license text in LICENSE.txt file)

Source code and precompiled binaries you can find on official repository:
https://bitbucket.org/Mrowqa/cargame-simple-sfml-2.0-game/

~ Mrowqa