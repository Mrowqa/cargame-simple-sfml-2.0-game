#include "Game.h"
#include <iostream>
#include <stdexcept>
using namespace std;

int main()
{
	srand(static_cast<unsigned int>(time(nullptr)));

	try 
	{
		Game game;
		game.run();
	}
	catch(std::runtime_error e)
	{
		cout << e.what() << endl;
	}

	return 0;
}