#include "GameObject.h"
#include <stdexcept>


GameObject::GameObject(void) : rotation(0)
{
}


void GameObject::loadTexture(std::string path)
{
	if(!texture.loadFromFile(path))
		throw std::runtime_error("GameObject::loadTexture - cannot load \"" + path + "\"");
	sprite.setTexture(texture);
	sf::Vector2u textureSize = texture.getSize();
	sprite.setOrigin(textureSize.x / 2.f, textureSize.y / 2.f);
}


void GameObject::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= sf::Transform().translate(pos).rotate(rotation);
	target.draw(sprite, states);
}
