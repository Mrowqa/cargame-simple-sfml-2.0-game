#pragma once

#include <SFML/Graphics.hpp>

class GameObject : public sf::Drawable
{
public:
	sf::Sprite sprite;
	sf::Vector2f pos;
	float rotation;

	/// Wczytuje tekstur� z podanej �cie�ki
	void loadTexture(std::string path);

	GameObject(void);

private:
	sf::Texture texture; /// Opcjonalne, wykorzystywane dla wczytania z pliku
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

