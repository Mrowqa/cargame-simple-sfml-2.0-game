#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Hero.h"
#include "Tank.h"
#include "PointAnimation.h"

class Game : private sf::NonCopyable
{
public:
	Game(void);
	~Game(void);

	void run();

private:
	void processEvents();
	void update(sf::Time elapsedTime);
	void render();

	void updateMovement(sf::Time elapsedTime);
	void updateCheckCollisions(sf::Time elapsedTime);
	void updateGui();
	void updateGameover();
	
	void handlePlayerInput(sf::Event::KeyEvent key, bool isPressed);
	void handlePlayerInput(sf::Event::JoystickButtonEvent jbEvent, bool isPressed);
	void handlePlayerInput(sf::Event::JoystickMoveEvent jmEvent);

	void useJoystick(bool use);

private:
	void init();
	void changePauseState(bool willPause);
	void calculateBlurRadius(sf::Time elapsedTime);

	static const sf::Time TimePerFrame;

	
	// Stan gry
	bool isGameover;
	bool usingFullscreen;
	bool isPaused;
	unsigned int currentJoystickId;
	sf::Clock gameClock;
	sf::Time gameTime;
	
	// GUI (wa�ne)
	unsigned int headerHeight;
	unsigned int footerHeight;
	std::string windowTitle;
	sf::Vector2u windowSize;
	sf::Vector2u sceneSize;

	// Zasoby gry
	sf::RenderWindow window;
	sf::RenderTexture scene;

	sf::Font gameFont;
	sf::SoundBuffer pickupBuff,
		gameoverBuff1,
		gameoverBuff2;
	sf::Sound pickupSound,
		gameoverSound1,
		gameoverSound2;
	sf::Music backgroundMusic;
	sf::Shader blurEffect;

	// GUI (cd)
	sf::RectangleShape header,
		barsBackground,
		fuelbar,
		nitrobar;
	sf::Text pointsString,
		tanksString,
		infoString,
		controlInfoString,
		gameover1String,
		gameover2String,
		timeString;


	// Entities
	sf::FloatRect playableArea;
	Hero hero;
	std::vector<Tank> tanks;
	PointAnimation pointAnimation;
	sf::CircleShape arrowSprite;
};

