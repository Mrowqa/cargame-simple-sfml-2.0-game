#include "PointAnimation.h"


PointAnimation::PointAnimation(void) : lifetime(5)
{
}


void PointAnimation::add(const Tank& pos)
{
	data.push_back(pos);
	data.back().clock.restart(); // Zrestartowanie zegara na cele animacji
}

void PointAnimation::update()
{
	for(unsigned int i=0; i<data.size(); i++)
	{
		// Usuni�cie obiektu, je�eli nie b�dzie ju� renderowany
		if(data[i].clock.getElapsedTime().asSeconds() > lifetime)
		{
			data.erase(data.begin()+i);
			i--;
			continue;
		}
	}
}

void PointAnimation::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for(unsigned int i=0; i<data.size(); i++)
	{
		// Wyliczenie szczeg��w do animacji
		sf::Vector2f labelPos;
		float time = data[i].clock.getElapsedTime().asSeconds();
		labelPos.x = data[i].pos.x + std::sin(time * 3) * (10 - time / 2) * 5;
		labelPos.y = data[i].pos.y - time * 15; // poruszanie si� po sinusoidzie w g�r�
		float scaleFactor = 1.f;
		if(time > 0.6f * lifetime) scaleFactor = (lifetime - time) / (lifetime * 0.4f); // zmniejszanie si�
		data[i].label.setColor(sf::Color(255, 255, 255, static_cast<sf::Uint8>(scaleFactor * 255))); // zanikanie

		sf::RenderStates tmpStates = states.transform * sf::Transform().translate(labelPos).scale(scaleFactor, scaleFactor);
		target.draw(data[i].label, tmpStates);
	}
}