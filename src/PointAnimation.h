#pragma once

#include <SFML/Graphics.hpp>
#include "Tank.h"

class PointAnimation : public sf::Drawable
{
public:
	const float lifetime; // w sekundach

	/// "Dodanie animacji"
	void add(const Tank& pos);

	/// Aktualizacja stanu animacja, powinna by� wo�ana przed rysowaniem
	void update();

	PointAnimation();

private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	mutable std::vector<Tank> data; // Dla prostoty -- dzi�ki temu unika si� zb�dnego powt�rzenia kodu
};

