#pragma once

#include <SFML/Graphics.hpp>
#include "GameObject.h"

class Hero : public GameObject
{
public:

	float tankCapacity; // w sekundach
	float nitroCapacity; // w sekundach
	float nitroLevel; // aktualny stan nitro
	static const float linearSpeed;
	static const float angularSpeed;

	unsigned int points; // zebrane pkt
	unsigned int tanks; // zebrane kanistry
	

	/// Pobierz stan baku - zakres [0 .. 1]
	float getFuelState() const;

	/// Pobierz stan nitro - zakres [0 .. 1]
	float getNitroState() const;

	/// Przywr�� pocz�tkowe warto�ci bohatera
	void restart();

	/// Nie odliczaj paliwa z baku
	void pause();

	/// Odliczaj paliwo z baku ponownie
	void resume();

	/// Zresetowanie stanu baku (nape�nienie)
	void fillTank();

	Hero(float x = 0, float y = 0, float rotation = 0);
	
	// Stan sterowania bohaterem
	bool isMovingUp;
	bool isMovingDown;
	bool isRotatingLeft;
	bool isRotatingRight;
	
	enum class UsingNitroState {
		NotUsing,
		UsingButOutOfNitro,
		Using
	} usingNitroState;

private:
	sf::Vector2f startPos;
	float startRotation;
	
	sf::Clock clock; // czas od ostatniego zebrania baku
	sf::Clock pauseClock;
	sf::Time pauseTime;
	bool isPaused;
};


///////////////////////////////////////////////////////
inline float Hero::getNitroState() const
{
	return std::max(nitroLevel / nitroCapacity, 0.f);
}
