#include "Tank.h"
#include <stdexcept>

sf::Texture Tank::texture;

Tank::Tank(void)
{
	if(texture.getSize().x == 0)
	{
		std::string texturePath = "../data/tank.png";
		if(!texture.loadFromFile(texturePath))
			throw std::runtime_error("Tank::(constructor) - cannot load \"" + texturePath + "\"");
	}
	sprite.setTexture(texture);
	sprite.scale(0.8f, 0.8f); // Przygotowana grafika jest nieco za du�a
	sf::Vector2u textureSize = texture.getSize();
	sprite.setOrigin(textureSize.x / 2.f, textureSize.y / 2.f);
}
