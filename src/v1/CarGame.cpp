/** 
 * ------ ENGLISH:
 *  This is simple SFML 2.0 game written as article addition for polish Programmer Magazine: http://programistamag.pl
 *  Source code licence: CC BY 3.0 http://creativecommons.org/licenses/by/3.0/
 * ------ POLSKI:
 *  Jest to prosta gra napisana przy pomocy biblioteki SFML 2.0 jako dodatek dla artyku�u napisanego dla Magazynu Programista: http://programistamag.pl
 *  Licencja kodu �r�d�owego: CC BY 3.0 http://creativecommons.org/licenses/by/3.0/pl/
 *
 * @author: Artur "Mrowqa" Jamro
 * 
 */

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <sstream>

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

#define DATA_DIR "../data/"

inline double deg2rad(float degrees)
{
	return degrees * (M_PI / 180.f);
}

// Klasa reprezentuj�ca bohatera gry
class Hero
{
public:
	sf::Vector2f pos;
	sf::Clock clock; // czas od ostatniego zebrania baku
	float rotation;

	float tankCapacity; // w sekundach
	float nitroCapacity; // w sekundach
	float nitroLevel; // aktualny stan nitro
	static const float linearSpeed;
	static const float angularSpeed;

	unsigned int points; // zebrane pkt
	unsigned int tanks; // zebrane baki
	
	// Pobierz stan baku - zakres [0 .. 1]
	float getFuelState() const
	{
		float state = 0;
		if(clock.getElapsedTime().asSeconds() < tankCapacity)
			state = (tankCapacity - clock.getElapsedTime().asSeconds()) / tankCapacity;
		return state;
	}

	// Pobierz stan nitro - zakres [0 .. 1]
	float getNitroState() const
	{
		return nitroLevel / nitroCapacity;
	}

	Hero(float x = 0, float y = 0, float rotation = 0) : startPos(sf::Vector2f(x, y)), startRotation(rotation) {restart();}

	void restart()
	{
		tankCapacity = 7;
		nitroCapacity = 3;
		nitroLevel = nitroCapacity;
		points = tanks = 0;
		clock.restart();
		pos = startPos;
		rotation = startRotation;
	}

private:
	sf::Vector2f startPos;
	float startRotation;
};
const float Hero::angularSpeed = 110;
const float Hero::linearSpeed = 225;

// Struktura przeznaczona dla bak�w - wykorzystywana przy animacji oraz naliczaniu punkt�w
struct TankData
{
	sf::Vector2f pos;
	sf::Clock clock;
	sf::Text label; // dla animacji
};

// Klasa zarz�dzaj�ca animacjami punkt�w
class PointAnimation
{
public:
	const float lifetime; // w sekundach

	// "Dodanie animacji"
	void add(const TankData& pos)
	{
		data.push_back(pos);
		data.back().clock.restart(); // Zrestartowanie zegara na cele animacji
	}

	// Renderowanie
	void draw(sf::RenderWindow& window)
	{
		for(unsigned int i=0; i<data.size(); i++)
		{
			// Usuni�cie obiektu, je�eli nie b�dzie ju� renderowany
			if(data[i].clock.getElapsedTime().asSeconds() > lifetime)
			{
				data.erase(data.begin()+i);
				i--;
				continue;
			}

			// Wyliczenie szczeg��w do animacji
			sf::Vector2f labelPos;
			float time = data[i].clock.getElapsedTime().asSeconds();
			labelPos.x = data[i].pos.x + std::sin(time * 3) * (10 - time / 2) * 5;
			labelPos.y = data[i].pos.y - time * 15; // poruszanie si� po sinusoidzie w g�r�
			float scaleFactor = 1.f;
			if(time > 0.6f * lifetime) scaleFactor = (lifetime - time) / (lifetime * 0.4f); // zmniejszanie si�
			data[i].label.setColor(sf::Color(255, 255, 255, static_cast<sf::Uint8>(scaleFactor * 255))); // zanikanie

			window.draw(data[i].label, sf::Transform().translate(labelPos).scale(scaleFactor, scaleFactor));
		}
	}

	PointAnimation() : lifetime(5) {}

private:
	std::vector<TankData> data;
};

int main()
{
	// Wczytanie grafik samochodu i baku
	sf::Texture carTexture, tankTexture;
	if(!carTexture.loadFromFile(DATA_DIR"car.png") || !tankTexture.loadFromFile(DATA_DIR"tank.png"))
		return -1;
	sf::Sprite car(carTexture), tank(tankTexture);
	car.setOrigin(car.getLocalBounds().width / 2, car.getLocalBounds().height / 2);
	tank.setOrigin(tank.getLocalBounds().width / 2, tank.getLocalBounds().height / 2);
	tank.scale(0.8f, 0.8f);
	car.rotate(90); // zwr�cenie ku g�rze
	Hero hero(400, 300, 270);

	// Wczytanie d�wi�k�w
	sf::SoundBuffer pickupBuff, gameoverBuff1, gameoverBuff2;
	pickupBuff.loadFromFile(DATA_DIR"tank_pickup.wav");
	gameoverBuff1.loadFromFile(DATA_DIR"game_over_1.wav");
	gameoverBuff2.loadFromFile(DATA_DIR"game_over_2.wav");
	sf::Sound pickupSound(pickupBuff),
		gameoverSound1(gameoverBuff1),
		gameoverSound2(gameoverBuff2);
	sf::Music backgroundMusic;
	backgroundMusic.openFromFile(DATA_DIR"background.ogg");
	backgroundMusic.setLoop(true);
	backgroundMusic.play();

	// Wczytanie czcionki i ustawienie podstawowych tekst�w
	sf::Font font;
	font.loadFromFile(DATA_DIR"DISTGRG_.ttf");
	sf::Text pointsString("Punkty: 0", font),
		tanksString("Baki: 0", font),
		infoString("Tab - Nitro\nAlt*Enter - Pelny ekran\nF1 - Uzywaj joysticka", font, 15),
		controlInfoString("Klawiatura", font),
		gameover1String("Game over", font, 70),
		gameover2String("Nacisnij Enter, by zagrac ponownie", font, 35);
	infoString.setPosition(10, 550);
	controlInfoString.setPosition(800 - controlInfoString.getLocalBounds().width - 10, 555);
	gameover1String.setPosition((800 - gameover1String.getLocalBounds().width) / 2, 260);
	gameover2String.setPosition((800 - gameover2String.getLocalBounds().width) / 2, 340);
	PointAnimation pointAnimation; // efekt unosz�cych si� punkt�w po zebraniu baku

	// Przygotowanie prymityw�w dla nag��wka i stopki
	sf::RectangleShape header(sf::Vector2f(800, 100)),
		barsBackground(sf::Vector2f(210, 30)),
		fuelbar(sf::Vector2f(200, 20)),
		nitrobar(sf::Vector2f(200, 20));
	header.setFillColor(sf::Color(87, 87, 87)); // Szary
	barsBackground.setPosition(sf::Vector2f(80, 20));
	barsBackground.setFillColor(sf::Color::Yellow);
	fuelbar.setPosition(sf::Vector2f(85, 25));
	fuelbar.setFillColor(sf::Color::Red);
	nitrobar.setPosition(sf::Vector2f(85, 60));
	nitrobar.setFillColor(sf::Color::Blue);

	// Rozstawienie bak�w po �wiecie
	sf::IntRect playableArea(10, 110, 780, 430); // Obszar po kt�rym mo�e porusza� si� gracz
	srand(time(nullptr));
	std::vector<TankData> tanks(3);
	for(auto& it : tanks)
	{
		it.pos.x = static_cast<float>(rand() % playableArea.width + playableArea.left); // [10 .. 789]
		it.pos.y = static_cast<float>(rand() % playableArea.height + playableArea.top); // [110 .. 539]
		it.label.setFont(font);
	}

	// Sta�e i zmienne potrzebne dla logiki gry
	const float secondsPerFrame = 1 / 60.f;
	bool gameover = false;
	bool useJoystick = false;
	bool usingNitro = false;
	unsigned int joystickID = 0;

	// Utworzenie okna
	sf::RenderWindow window(sf::VideoMode(800, 600), "Prosta gra w SFMLu");
	window.setFramerateLimit(60); // Ograniczenie do 60 FPS
	window.setMouseCursorVisible(false); // Ukryj kursor myszy
	bool fullscreen = false;

	while(window.isOpen())
	{
		// ----------- P�tla komunikat�w ----------------------------
		sf::Event event;
		while(window.pollEvent(event))
		{
			switch(event.type)
			{
			case sf::Event::Closed: // Wyjd� z gry
				window.close();
				break;

			case sf::Event::KeyPressed:
				if(event.key.code == sf::Keyboard::Escape) // Wyjd� z gry
				{
					window.close();
					if(fullscreen)
					{
						window.create(sf::VideoMode(800, 600), "Prosta gra w SFMLu");
						fullscreen = false;
					}
				}
				else if(event.key.code == sf::Keyboard::Return)
				{
					if(event.key.alt) // Pelny ekran <-> tryb okna
					{
						fullscreen = !fullscreen;
						window.close();
						window.create(sf::VideoMode(800, 600), "Prosta gra w SFMLu", sf::Style::Close | sf::Style::Resize |
							sf::Style::Titlebar | (fullscreen ? sf::Style::Fullscreen : sf::Style::None));
					}
					else if(gameover) // Zrestartuj gr�
					{
						gameover = false;
						hero.restart();
						for(auto& it : tanks)
							it.clock.restart();
					}
				}
				else if(event.key.code == sf::Keyboard::F1) // Prze��cz sterowanie Klawiatura <-> Joystick
				{
					if(useJoystick) useJoystick = false;
					else
					{
						for(unsigned int i = 0; i < 8; i++)
							if(sf::Joystick::isConnected(i) && sf::Joystick::hasAxis(i, sf::Joystick::Axis::X) &&
								sf::Joystick::hasAxis(i, sf::Joystick::Axis::Y) && sf::Joystick::getButtonCount(i) >= 1)
							{
								useJoystick = true;
								joystickID = i;
								break;
							}
					}

					if(useJoystick)
					{
						infoString.setString("Przycisk 0 - Nitro\nAlt*Enter - Pelny ekran\nF1 - Uzywaj klawiatury");
						controlInfoString.setString(std::string("Joystick ") + static_cast<char>('0' + joystickID));
						controlInfoString.setPosition(800 - controlInfoString.getLocalBounds().width - 10, 555);
					}
					else
					{
						infoString.setString("Tab - Nitro\nAlt*Enter - Pelny ekran\nF1 - Uzywaj joysticka");
						controlInfoString.setString("Klawiatura");
						controlInfoString.setPosition(800 - controlInfoString.getLocalBounds().width - 10, 555);
					}
				}
				break;

			case sf::Event::JoystickDisconnected:
				if(event.joystickConnect.joystickId == joystickID) // Je�eli od��czono aktualny joystick, to zmie� sterowanie na klawiatur�
				{
					useJoystick = false;
					infoString.setString("Tab - Nitro\nAlt*Enter - Pelny ekran\nF1 - Uzywaj joysticka");
					controlInfoString.setString("Klawiatura");
					controlInfoString.setPosition(800 - controlInfoString.getLocalBounds().width - 10, 555);
				}
				break;
			}
		}



		// ------------- Logika gry -------------------------
		if(gameover || hero.getFuelState() == 0) // game over!
		{
			if(!gameover) // Za pierwszym razem odegraj d�wi�ki
			{
				gameover = true;
				gameoverSound1.play();
				gameoverSound2.play();
				hero.clock.restart();
			}

			// Animowanie napis�w
			float time = hero.clock.getElapsedTime().asSeconds();
			float factor = std::min(1.f, time / 3.f);
			
			gameover1String.setColor(sf::Color(255, 255, 255, static_cast<sf::Uint8>(factor * 255)));
			gameover2String.setColor(sf::Color(255, 255, 255, static_cast<sf::Uint8>(factor * 255)));
			gameover1String.setScale(2 - factor, 2 - factor);
		}
		else
		{
			// Czy gracz u�ywa nitro?
			float speedFactor = 1.f;
			usingNitro = false;
			bool isNitroButtonPushed = useJoystick ? sf::Joystick::isButtonPressed(joystickID, 0) : sf::Keyboard::isKeyPressed(sf::Keyboard::Tab);
			if(hero.nitroLevel > 0 && isNitroButtonPushed)
			{
				speedFactor = 1.75f;
				hero.nitroLevel -= secondsPerFrame;
				usingNitro = true;
			}

			// Skr�t
			if(useJoystick)
			{
				float move = sf::Joystick::getAxisPosition(joystickID, sf::Joystick::Axis::X) / 100.f;
				hero.rotation += hero.angularSpeed * secondsPerFrame * speedFactor * move;
			}
			else
			{
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
					hero.rotation -= hero.angularSpeed * secondsPerFrame * speedFactor;
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
					hero.rotation += hero.angularSpeed * secondsPerFrame * speedFactor;
			}
			while(hero.rotation < 0) hero.rotation += 360;
			while(hero.rotation > 360) hero.rotation -= 360;

			// Gaz i wsteczny
			if(useJoystick)
			{
				float move = -sf::Joystick::getAxisPosition(joystickID, sf::Joystick::Axis::Y) / 100.f;
				hero.pos.x += static_cast<float>(hero.linearSpeed * secondsPerFrame * speedFactor * std::cos(deg2rad(hero.rotation))) * move;
				hero.pos.y += static_cast<float>(hero.linearSpeed * secondsPerFrame * speedFactor * std::sin(deg2rad(hero.rotation))) * move;
			}
			else
			{
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
				{
					hero.pos.x += static_cast<float>(hero.linearSpeed * secondsPerFrame * speedFactor * std::cos(deg2rad(hero.rotation)));
					hero.pos.y += static_cast<float>(hero.linearSpeed * secondsPerFrame * speedFactor * std::sin(deg2rad(hero.rotation)));
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
				{
					hero.pos.x -= static_cast<float>(hero.linearSpeed * secondsPerFrame * speedFactor * std::cos(deg2rad(hero.rotation)));
					hero.pos.y -= static_cast<float>(hero.linearSpeed * secondsPerFrame * speedFactor * std::sin(deg2rad(hero.rotation)));
				}
			}
			if(hero.pos.x < playableArea.left) hero.pos.x = static_cast<float>(playableArea.left);
			if(hero.pos.x > playableArea.left + playableArea.width) hero.pos.x = static_cast<float>(playableArea.left + playableArea.width);
			if(hero.pos.y < playableArea.top) hero.pos.y = static_cast<float>(playableArea.top);
			if(hero.pos.y > playableArea.top + playableArea.height) hero.pos.y = static_cast<float>(playableArea.top + playableArea.height);

			// Wykrycie kolizji - niedok�adna bazowana na AABB
			sf::FloatRect heroRect = car.getLocalBounds();
			heroRect.left += hero.pos.x;
			heroRect.top += hero.pos.y;
			for(auto& it : tanks)
			{
				sf::FloatRect tankRect = tank.getLocalBounds();
				tankRect.left += it.pos.x;
				tankRect.top += it.pos.y;

				if(heroRect.intersects(tankRect)) // Wykryto kolizj�!
				{
					hero.clock.restart(); // Uzupe�nienie baku gracza
					if(hero.tankCapacity > 3) // Utrudnienie - zmniejsza si� pojemno�� baku
						hero.tankCapacity *= 0.95f;
					hero.nitroCapacity += 0.01f; // bonus - 100 bak�w = 1 sekunda do max nitro
					hero.nitroLevel = std::min(hero.nitroLevel + 0.3f, hero.nitroCapacity); // Otrzymanie czasu nitro

					if(pickupSound.getStatus() == sf::Sound::Playing) pickupSound.setPlayingOffset(sf::seconds(0));
					else pickupSound.play();
				
					// Liczba zdobytych pkt: czas<1s - 100pkt; czas [1 .. 6]s - [100 .. 16] pkt; czas>6s - 10pkt
					float time = it.clock.getElapsedTime().asSeconds();
					int gainedPoints = 10;
					if(time < 1.f) gainedPoints = 100;
					else if(time < 6) gainedPoints = static_cast<int>((7 - time) * (1/6.f * 100));

					// Aktualizacja statystyk
					hero.points += gainedPoints;
					hero.tanks++;

					std::ostringstream str;
					str << "* " << gainedPoints << " *";
					it.label.setString(str.str());
					pointAnimation.add(it); // Dodanie animacji

					// Postawienie baku w nowe miejsce
					it.pos.x = static_cast<float>(rand() % playableArea.width + playableArea.left); // [10 .. 789]
					it.pos.y = static_cast<float>(rand() % playableArea.height + playableArea.top); // [110 .. 539]
					it.clock.restart(); // Zrestartowanie licznika baku
				}
			}

			// Przygotowanie danych do renderowania
			fuelbar.setScale(hero.getFuelState(), 1.f); // Pasek baku
			nitrobar.setScale(hero.getNitroState(), 1.f); // Pasek nitro

			std::ostringstream str;
			str << "Punkty: " << hero.points;
			pointsString.setString(str.str());
			pointsString.setPosition((500 - pointsString.getLocalBounds().width) / 2 + 300, 10);

			str.str(""); // wyczyszczenie bufora
			str << "Baki: " << hero.tanks;
			tanksString.setString(str.str());
			tanksString.setPosition((500 - tanksString.getLocalBounds().width) / 2 + 300, 50);
		}


		// --------------- Renderowanie sceny -------------------------
		window.clear(sf::Color(255, 127, 39)); // Pomara�czowy

		window.draw(car, sf::Transform().translate(hero.pos).rotate(hero.rotation));
		if(usingNitro) window.draw(car, sf::Transform().translate(hero.pos).rotate(hero.rotation + 180).scale(0.5f, 0.5f));
		for(auto& it : tanks)
			window.draw(tank, sf::Transform().translate(it.pos));
		pointAnimation.draw(window);

		// Renderowanie nag��wka
		window.draw(header);
		window.draw(tank, sf::Transform().translate(40, 50));
		window.draw(barsBackground); // Pod pasek baku
		window.draw(barsBackground, sf::Transform().translate(0, 35)); // Pod pasek nitro
		window.draw(fuelbar);
		window.draw(nitrobar);
		window.draw(pointsString);
		window.draw(tanksString);

		// Renderowanie stopki
		window.draw(header, sf::Transform().translate(0, 550));
		window.draw(infoString);
		window.draw(controlInfoString);

		// Animacja napis�w pod koniec gry
		if(gameover)
		{
			window.draw(gameover1String);
			window.draw(gameover2String);
		}

		window.display();
	}


	return 0;
}