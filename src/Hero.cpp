#include "Hero.h"

const float Hero::angularSpeed = 110;
const float Hero::linearSpeed = 225;

Hero::Hero(float x, float y, float rotation)
	: startPos(sf::Vector2f(x, y)), startRotation(rotation),
	isMovingDown(false), isMovingUp(false),
	isRotatingLeft(false), isRotatingRight(false),
	usingNitroState(UsingNitroState::NotUsing),
	isPaused(false)
{
	restart();
	loadTexture("../data/car.png");
	sprite.rotate(90); // Aby sprajt by� skierowany zgodnie ze sk�adow� rotation
}

float Hero::getFuelState() const
{
	float state = 0;
	sf::Time timePast = clock.getElapsedTime() - pauseTime - (isPaused ? pauseClock.getElapsedTime() : sf::Time::Zero);
	if(timePast.asSeconds() < tankCapacity)
		state = (tankCapacity - timePast.asSeconds()) / tankCapacity;
	return std::max(state, 0.f);
}

void Hero::restart()
{
	tankCapacity = 9.f;
	nitroCapacity = 4.f;
	nitroLevel = nitroCapacity;
	points = tanks = 0;
	clock.restart();
	pauseClock.restart();
	pos = startPos;
	rotation = startRotation;
	pauseTime = sf::Time::Zero;
}

void Hero::pause()
{
	if(isPaused)
		return;

	isPaused = true;
	pauseClock.restart();
}

void Hero::resume()
{
	if(!isPaused)
		return;

	isPaused = false;
	pauseTime += pauseClock.getElapsedTime();
}

void Hero::fillTank()
{
	clock.restart();
	pauseClock.restart();
	pauseTime = sf::Time::Zero;
}