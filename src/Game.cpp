#include "Game.h"
#include <sstream>
#include <stdexcept>
#include <cmath>
#include <iostream>

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

inline float deg2rad(float degrees)
{
	return degrees * static_cast<float>(M_PI / 180.f);
}

inline float rad2deg(float radians)
{
	return radians * static_cast<float>(180.f / M_PI);
}


const sf::Time Game::TimePerFrame = sf::seconds(1.f/60.f);

Game::Game(void)
	: windowSize(800, 600),
	sceneSize(windowSize.x, windowSize.y - headerHeight - footerHeight),
	window(sf::VideoMode(windowSize.x, windowSize.y), windowTitle),
	playableArea(0, 0, 1200, 800),
	hero(playableArea.left + playableArea.width / 2, playableArea.top + playableArea.height / 2, 270.f),
	isGameover(false),
	isPaused(false),
	usingFullscreen(false),
	currentJoystickId(-1),
	
	windowTitle("CarGame 2 - Simple SFML Game"),
	headerHeight(100),
	footerHeight(65),
	header(sf::Vector2f(static_cast<float>(windowSize.x), static_cast<float>(headerHeight))),
	barsBackground(sf::Vector2f(210, 30)),
	fuelbar(sf::Vector2f(200, 20)),
	nitrobar(sf::Vector2f(200, 20)),

	pointsString("Punkty: 0", gameFont),
	tanksString("Kanistry: 0", gameFont),
	infoString("Tab - Nitro\nP - Pauza\nAlt*Enter - Pelny ekran\nF1 - Uzywaj joysticka", gameFont, 15), // dlaczego ustawienie stringu w konstruktorze nie dzia�a?
	controlInfoString("Klawiatura", gameFont),
	gameover1String("Game over", gameFont, 70),
	gameover2String("Nacisnij Enter, by zagrac ponownie", gameFont, 35),
	timeString("00:00:00.0000", gameFont, 30)

{
	init();
}

Game::~Game(void)
{
}

void Game::init()
{
	std::string fontPath = "../data/DISTGRG_.ttf";
	if(!gameFont.loadFromFile(fontPath))
			throw std::runtime_error("Game::init - cannot load \"" + fontPath + "\"");

	if(!pickupBuff.loadFromFile("../data/tank_pickup.wav") ||
		!gameoverBuff1.loadFromFile("../data/game_over_1.wav") ||
		!gameoverBuff2.loadFromFile("../data/game_over_2.wav") ||
		!backgroundMusic.openFromFile("../data/background.ogg"))
			throw std::runtime_error("Game::init - cannot load one of sounds or open music");

	pickupSound.setBuffer(pickupBuff);
	gameoverSound1.setBuffer(gameoverBuff1);
	gameoverSound2.setBuffer(gameoverBuff2);
	backgroundMusic.setLoop(true);

	tanks.resize(4);
	for(auto& it : tanks)
	{
		it.pos.x = static_cast<float>(rand() % static_cast<int>(playableArea.width) + playableArea.left);
		it.pos.y = static_cast<float>(rand() % static_cast<int>(playableArea.height) + playableArea.top);
		it.label.setFont(gameFont);
	}

	// Przygotowanie GUI z prymityw�w
	header.setFillColor(sf::Color(87, 87, 87)); // Szary
	barsBackground.setPosition(sf::Vector2f(80, 20));
	barsBackground.setFillColor(sf::Color::Yellow);
	fuelbar.setPosition(sf::Vector2f(85, 25));
	fuelbar.setFillColor(sf::Color::Red);
	nitrobar.setPosition(sf::Vector2f(85, 60));
	nitrobar.setFillColor(sf::Color::Blue);
	

	infoString.setString("Tab - Nitro\nP - Pauza\nAlt*Enter - Pelny ekran\nF1 - Uzywaj joysticka");
	controlInfoString.setString("Klawiatura");
	gameover1String.setString("Game over");
	gameover2String.setString("Nacisnij Enter, by zagrac ponownie");
		
	infoString.setPosition(10, static_cast<float>(windowSize.y - footerHeight));
	controlInfoString.setPosition(windowSize.x - controlInfoString.getLocalBounds().width - 10.f, windowSize.y - 52.f);
	gameover1String.setPosition((windowSize.x - gameover1String.getLocalBounds().width) / 2, windowSize.y / 2.f - 40);
	gameover2String.setPosition((windowSize.x - gameover2String.getLocalBounds().width) / 2, windowSize.y / 2.f + 40);

	arrowSprite.setRadius(10.f);
	arrowSprite.setPointCount(3);
	arrowSprite.setFillColor(sf::Color::Blue);
	sf::FloatRect arrowBounds = arrowSprite.getLocalBounds();
	arrowSprite.setOrigin(arrowBounds.width / 2.f, arrowBounds.height + hero.sprite.getLocalBounds().height / 2.f + 10.f);
	//sprawdz wsparcie dla shaderow

	if(sf::Shader::isAvailable())
	{
		std::string shaderPath = "../data/blur.frag";
		if(!blurEffect.loadFromFile(shaderPath, sf::Shader::Fragment))
				throw std::runtime_error("Game::init - cannot load \"" + shaderPath + "\"");
		blurEffect.setParameter("texture", sf::Shader::CurrentTexture);
	}
	else
		std::cout << "Your system doesn't support shaders!\n";

	if(!scene.create(sceneSize.x, sceneSize.y))
			throw std::runtime_error("Game::init - cannot create texture");
}


void Game::run()
{
	backgroundMusic.play();

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		sf::Time elapsedTime = clock.restart();
		timeSinceLastUpdate += elapsedTime;
		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;

			processEvents();
			update(TimePerFrame);
		}
		render();
	}
}

void Game::processEvents()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		switch (event.type)
		{
			case sf::Event::KeyPressed:
				handlePlayerInput(event.key, true);
				break;

			case sf::Event::KeyReleased:
				handlePlayerInput(event.key, false);
				break;

			case sf::Event::JoystickButtonPressed:
				handlePlayerInput(event.joystickButton, true);
				break;

			case sf::Event::JoystickButtonReleased:
				handlePlayerInput(event.joystickButton, false);
				break;

			case sf::Event::JoystickMoved:
				handlePlayerInput(event.joystickMove);
				break;

			case sf::Event::JoystickDisconnected:
				useJoystick(false);
				break;

			case sf::Event::Closed:
				window.close();
				break;
		}
	}
}

void Game::update(sf::Time elapsedTime)
{
	if(isGameover || hero.getFuelState() == 0)
		updateGameover();
	else
	{
		if(!isPaused)
		{
			updateMovement(elapsedTime);
			updateCheckCollisions(elapsedTime);
		}
		updateGui();
	}
	pointAnimation.update();
}

void Game::render()
{
	window.clear();

	sf::Color background(255, 127, 39);
	scene.clear(background);
	
	float viewX = std::max(0.f, std::min(hero.pos.x - sceneSize.x / 2.f, playableArea.width - sceneSize.x));
	float viewY = std::max(0.f, std::min(hero.pos.y - sceneSize.y / 2.f, playableArea.height - sceneSize.y));
	sf::View sceneView(sf::FloatRect(viewX, viewY, static_cast<float>(sceneSize.x), static_cast<float>(sceneSize.y)));
	scene.setView(sceneView);

	for(auto& it : tanks)
		scene.draw(it);
	scene.draw(hero);
	if(hero.usingNitroState == Hero::UsingNitroState::Using)
		//scene.draw(hero, sf::Transform().rotate(180).scale(0.5f, 0.5f)); // FIXME powinno dzia�a�! -- nie przekazuje transformacji do metody renderuj�cej // bug MSVC - u�yj wtedy linii ni�ej
		scene.draw(hero.sprite, sf::Transform().translate(hero.pos).rotate(hero.rotation + 180).scale(0.5f, 0.5f));
	scene.display();

	
	sf::Sprite sceneSprite(scene.getTexture());
	sceneSprite.setPosition(0, static_cast<float>(headerHeight));
	if(sf::Shader::isAvailable())
		window.draw(sceneSprite, &blurEffect);
	else
		window.draw(sceneSprite);

	sf::Transform sceneTransfrom;
	sceneTransfrom.translate(0, static_cast<float>(headerHeight)).translate(-viewX, -viewY);
	window.draw(pointAnimation, sceneTransfrom);
	for(auto& it : tanks)
	{
		sf::Vector2f deltaPos = it.pos - hero.pos;
		float arrowScale = 0.7f + sqrt(deltaPos.x*deltaPos.x + deltaPos.y*deltaPos.y) / std::min(playableArea.height, playableArea.width) * 1.5f;
		float angle = rad2deg(atan2f(deltaPos.y, deltaPos.x)) + 90.f;
		window.draw(arrowSprite, sf::Transform(sceneTransfrom).translate(hero.pos).rotate(angle).scale(1.f, arrowScale));
	}

	// Renderowanie nag��wka
	window.draw(header);
	window.draw(tanks[0].sprite, sf::Transform().translate(40, 50));
	window.draw(barsBackground); // Pod pasek baku
	window.draw(barsBackground, sf::Transform().translate(0, 35)); // Pod pasek nitro
	window.draw(fuelbar);
	window.draw(nitrobar);
	window.draw(pointsString);
	window.draw(tanksString);
			
	// Renderowanie stopki
	window.draw(header, sf::Transform().translate(0, windowSize.y - footerHeight));
	window.draw(infoString);
	window.draw(controlInfoString);
	window.draw(timeString);
			
	// Animacja napis�w pod koniec gry
	if(isGameover) // dlaczego nie renderuje?
	{
		window.draw(gameover1String);
		window.draw(gameover2String);
	}
	else if(isPaused)
	{
		sf::Text pauseText("Pause", gameFont, 75);
		pauseText.setPosition((windowSize.x - pauseText.getLocalBounds().width - 10.f) / 2, (windowSize.y - 75.f) / 2);
		window.draw(pauseText);
	}

	window.display();
}

void Game::handlePlayerInput(sf::Event::KeyEvent key, bool isPressed)
{
	if(key.code == sf::Keyboard::Escape) // Wyjd� z gry
	{
		window.close();
		if(usingFullscreen)
		{
			window.create(sf::VideoMode(800, 600), windowTitle);
			usingFullscreen = false;
		}
	}
	else if(key.code == sf::Keyboard::Return)
	{
		if(key.alt) // Pelny ekran <-> tryb okna
		{
			usingFullscreen = !usingFullscreen;
			window.close();
			window.create(sf::VideoMode(800, 600), windowTitle, sf::Style::Close | sf::Style::Resize |
				sf::Style::Titlebar | (usingFullscreen ? sf::Style::Fullscreen : sf::Style::None));
		}
		else if(isGameover) // Zrestartuj gr�
		{
			isGameover = false;
			hero.restart();
			for(auto& it : tanks)
				it.clock.restart();
			gameClock.restart();
			gameTime = sf::Time::Zero;
		}
	}
	else if(key.code == sf::Keyboard::P && isPressed == false)
		changePauseState(!isPaused);
	else if(key.code == sf::Keyboard::F1 && isPressed == false)
		useJoystick(currentJoystickId == -1 ? true : false);

	else if(currentJoystickId == -1)
	{
		if(key.code == sf::Keyboard::Up)
			hero.isMovingUp = isPressed;
		else if(key.code == sf::Keyboard::Down)
			hero.isMovingDown = isPressed;
		else if(key.code == sf::Keyboard::Left)
			hero.isRotatingLeft = isPressed;
		else if(key.code == sf::Keyboard::Right)
			hero.isRotatingRight = isPressed;
		else if(key.code == sf::Keyboard::Tab)
			hero.usingNitroState = isPressed ? Hero::UsingNitroState::Using : Hero::UsingNitroState::NotUsing;
	}
}

void Game::handlePlayerInput(sf::Event::JoystickButtonEvent jbEvent, bool isPressed)
{
	if(jbEvent.joystickId != currentJoystickId)
		return;

	if(jbEvent.button == 0)
		hero.usingNitroState = isPressed ? Hero::UsingNitroState::Using : Hero::UsingNitroState::NotUsing;
}
void Game::handlePlayerInput(sf::Event::JoystickMoveEvent jmEvent)
{
	if(jmEvent.joystickId != currentJoystickId)
		return;

	switch(jmEvent.axis)
	{
	case sf::Joystick::Axis::Y:
		hero.isMovingUp = hero.isMovingDown = false;
		if(jmEvent.position < -30.f)
			hero.isMovingUp = true;
		else if(jmEvent.position > 30.f)
			hero.isMovingDown = true;
		break;
		
	case sf::Joystick::Axis::X:
		hero.isRotatingLeft = hero.isRotatingRight = false;
		if(jmEvent.position < -30.f)
			hero.isRotatingLeft = true;
		else if(jmEvent.position > 30.f)
			hero.isRotatingRight = true;
		break;

	default:
		break;
	}
}

void Game::useJoystick(bool use)
{
	if(use == true)
	{
		for(unsigned int i = 0; i < sf::Joystick::Count; i++)
			if(sf::Joystick::isConnected(i) && sf::Joystick::hasAxis(i, sf::Joystick::Axis::X) &&
				sf::Joystick::hasAxis(i, sf::Joystick::Axis::Y) && sf::Joystick::getButtonCount(i) >= 1)
				currentJoystickId = i;

		
		infoString.setString("Przycisk 0 - Nitro\nP - Pauza\nAlt*Enter - Pelny ekran\nF1 - Uzywaj klawiatury");
		controlInfoString.setString(std::string("Joystick ") + static_cast<char>('0' + currentJoystickId));
		controlInfoString.setPosition(windowSize.x - controlInfoString.getLocalBounds().width - 10.f, windowSize.y - 52.f);
	}
	else
	{
		currentJoystickId = -1;

		infoString.setString("Tab - Nitro\nP - Pauza\nAlt*Enter - Pelny ekran\nF1 - Uzywaj joysticka");
		controlInfoString.setString("Klawiatura");
		controlInfoString.setPosition(windowSize.x - controlInfoString.getLocalBounds().width - 10.f, windowSize.y - 52.f);
	}
}

void Game::updateMovement(sf::Time elapsedTime)
{
	sf::Vector2f movement(0.f, 0.f);
	float rotation = 0.f;
	if(hero.isRotatingLeft)
		rotation = -Hero::angularSpeed;
	if(hero.isRotatingRight)
		rotation = Hero::angularSpeed;

	if(hero.isMovingUp)
	{
		movement.x += Hero::linearSpeed * std::cos(deg2rad(hero.rotation));
		movement.y += Hero::linearSpeed * std::sin(deg2rad(hero.rotation));
	}
	if(hero.isMovingDown)
	{
		movement.x -= Hero::linearSpeed * std::cos(deg2rad(hero.rotation));
		movement.y -= Hero::linearSpeed * std::sin(deg2rad(hero.rotation));
	}

	float speedFactor = 1.f;
	if(hero.usingNitroState != Hero::UsingNitroState::NotUsing)
	{
		if(hero.nitroLevel > 0)
		{
			speedFactor = 1.9f;
			hero.nitroLevel -= TimePerFrame.asSeconds();
			hero.usingNitroState = Hero::UsingNitroState::Using;
		}
		else
			hero.usingNitroState = Hero::UsingNitroState::UsingButOutOfNitro;
	}
	calculateBlurRadius(elapsedTime);

	hero.pos += movement * elapsedTime.asSeconds() * speedFactor;
	hero.pos.x = std::max(hero.pos.x, playableArea.left);
	hero.pos.x = std::min(hero.pos.x, playableArea.left + playableArea.width);
	hero.pos.y = std::max(hero.pos.y, playableArea.top);
	hero.pos.y = std::min(hero.pos.y, playableArea.top + playableArea.height);

	hero.rotation += rotation * elapsedTime.asSeconds() * speedFactor;
	hero.rotation = fmodf(hero.rotation, 360.f);
	if(hero.rotation < 0) hero.rotation += 360.f;
}

void Game::updateCheckCollisions(sf::Time elapsedTime)
{
	// Wykrycie kolizji - niedok�adna bazowana na AABB
	sf::FloatRect heroRect = hero.sprite.getLocalBounds();
	heroRect.left += hero.pos.x;
	heroRect.top += hero.pos.y;
	for(auto& it : tanks)
	{
		sf::FloatRect tankRect = it.sprite.getLocalBounds();
		tankRect.left += it.pos.x;
		tankRect.top += it.pos.y;

		if(heroRect.intersects(tankRect)) // Wykryto kolizj�!
		{
			hero.fillTank(); // Uzupe�nienie baku gracza
			if(hero.tankCapacity > 3.5f) // Utrudnienie - zmniejsza si� pojemno�� baku
				hero.tankCapacity *= 0.95f;
			hero.nitroCapacity += 0.015f; // bonus - ~66 bak�w = 1 sekunda do max nitro
			hero.nitroLevel = std::min(hero.nitroLevel + 0.35f, hero.nitroCapacity); // Otrzymanie czasu nitro

			if(pickupSound.getStatus() == sf::Sound::Playing) pickupSound.setPlayingOffset(sf::seconds(0));
			else pickupSound.play();
				
			// Liczba zdobytych pkt: czas<2s - 100pkt; czas [2 .. 10]s - [100 .. 11] pkt; czas>10s - 10pkt
			float time = it.clock.getElapsedTime().asSeconds();
			int gainedPoints = 10;
			if(time < 2.f) gainedPoints = 100;
			else if(time <= 10.f) gainedPoints = static_cast<int>(-(time - 11) * (100/9.f));

			// Aktualizacja statystyk
			hero.points += gainedPoints;
			hero.tanks++;

			std::ostringstream str;
			str << "* " << gainedPoints << " *";
			it.label.setString(str.str());
			pointAnimation.add(it); // Dodanie animacji

			// Postawienie baku w nowe miejsce
			it.pos.x = static_cast<float>(rand() % static_cast<int>(playableArea.width) + playableArea.left);
			it.pos.y = static_cast<float>(rand() % static_cast<int>(playableArea.height) + playableArea.top);
			it.clock.restart(); // Zrestartowanie licznika baku
		}
	}
}

void Game::updateGui()
{
	// Przygotowanie danych do renderowania
	fuelbar.setScale(hero.getFuelState(), 1.f); // Pasek baku
	nitrobar.setScale(hero.getNitroState(), 1.f); // Pasek nitro

	std::ostringstream str;
	str << "Punkty: " << hero.points;
	pointsString.setString(str.str());
	pointsString.setPosition((windowSize.x - 300 - pointsString.getLocalBounds().width) / 2 + 300, 10);

	str.str(""); // wyczyszczenie bufora
	str << "Kanistry: " << hero.tanks;
	tanksString.setString(str.str());
	tanksString.setPosition((windowSize.x - 300 - tanksString.getLocalBounds().width) / 2 + 300, 50);

	sf::Time deltaTime = gameClock.restart();
	if(!isPaused)
		gameTime += deltaTime;

	int miliseconds = gameTime.asMilliseconds();
	int hours =		miliseconds / (1000 * 60 * 60);
	int minutes =	miliseconds / (1000 * 60) % 60;
	int seconds =	miliseconds / 1000 % 60;

	str.str("");
	str << (hours < 10 ? "0" : "") << hours << ":"
		<< (minutes < 10 ? "0" : "") << minutes << ":"
		<< (seconds < 10 ? "0" : "") << seconds;
	
	timeString.setString(str.str());
	timeString.setPosition((windowSize.x - timeString.getLocalBounds().width) / 2, windowSize.y - (footerHeight + 35) / 2.f);
}


void Game::updateGameover()
{
	if(!isGameover) // Za pierwszym razem odegraj d�wi�ki
	{
		updateGui();

		isGameover = true;
		gameoverSound1.play();
		gameoverSound2.play();
		hero.fillTank();
	}
	
	// Animowanie napis�w
	float time = (1.f - hero.getFuelState()) * 5; // [1..0] -> [0..5]
	float factor = std::min(1.f, time / 3.f);
		
	gameover1String.setColor(sf::Color(255, 255, 255, static_cast<sf::Uint8>(factor * 255)));
	gameover2String.setColor(sf::Color(255, 255, 255, static_cast<sf::Uint8>(factor * 255)));
	gameover1String.setScale(2 - factor, 2 - factor);
}


void Game::changePauseState(bool willPause)
{
	isPaused = isGameover ? false : willPause;
	if(isPaused)
		hero.pause();
	else
		hero.resume();
}

void Game::calculateBlurRadius(sf::Time elapsedTime)
{
	static float blurRadius = 0.f;
	if(sf::Shader::isAvailable())
	{
		if(hero.usingNitroState == Hero::UsingNitroState::Using)
			blurRadius = std::min(blurRadius + elapsedTime.asSeconds() / 300.f, 0.01f);
		else
			blurRadius = std::max(blurRadius - elapsedTime.asSeconds() / 100.f, 0.f);
		blurEffect.setParameter("blur_radius", blurRadius);
	}
}